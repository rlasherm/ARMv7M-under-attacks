This application is an AES on STM32. Communication with the host computer is done with UART. We use the FTDIFriend module from ADAFruit to have UART2USB conversion.

Here we try to add a buffer overflow vulnerability to a method that should not be vulnerable.


If fault occur at the right point:
cipher = 6F 0A 9F FF CB 98 DF 4F 74 25 F7 3D 09 BB DD FB
key = 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 10
