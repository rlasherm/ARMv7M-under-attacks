/**
 * @Author: Lashermes Ronan <ronan>
 * @Date:   01-06-2017
 * @Email:  ronan.lashermes@inria.fr
 * @Last modified by:   ronan
 * @Last modified time: 01-06-2017
 * @License: MIT
 */



#include "buf_ov.h"
#include "stm32f1xx_hal.h"

//char big_text[256+48];

void init_big_text() {
  int i, len = 0;
  //big_text[0] = 1;
  for(i = len; i < sizeof(big_text); i++) {
    big_text[i] = (char)((i%128)+1);
  }
  big_text[sizeof(big_text)-1] = '\0';
}


// void wait (int counter){
//   int start = HAL_GetTick();
//   while((HAL_GetTick() - start) < counter);
// }

void wait (int counter){
  int start, current;
  start = HAL_GetTick();
  start += counter;
  current = HAL_GetTick();
  while(start < current)
  {
    current = HAL_GetTick();
  }

}

void copy_text(char* dest, size_t max_dest_size) {
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);
  strncpy(dest, big_text, max_dest_size);
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
}
