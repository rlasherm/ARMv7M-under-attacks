using Plotly
using Rsvg

function shape()
  path_status = "../results/status_machine.csv"
  data_status = readcsv(path_status, String)

  path_crashs = "../results/crashs_machine.csv"
  data_crashs = readcsv(path_crashs, String)

  # Count how many highjacks (55) and how many normal (FF)
  status_countFF = Dict{Float64,Int64}()
  status_count55 = Dict{Float64,Int64}()
  st_count = size(data_status,1)
  for i in 1:st_count
    t = parse(Float64, data_status[i,1])
    status_countFF[t] = 0
    status_count55[t] = 0
  end

  for i in 1:st_count
    t = parse(Float64, data_status[i,1])
    if data_status[i,2] == "FFFFFFFF"
      status_countFF[t] += 1
    elseif data_status[i,2] == "55555555"
      status_count55[t] += 1
    end
  end

  # Count how many crashes
  cr_count = size(data_crashs,1)
  crashs_count = Dict{Float64,Int64}()
  for i in 1:cr_count
    t = parse(Float64, data_crashs[i,1])
    crashs_count[t] = 0
  end
  for i in 1:cr_count
    t = parse(Float64, data_crashs[i,1])
    crashs_count[t] += 1
  end

  #Create bar chart
  x55=collect(keys(status_count55))
  y55=collect(values(status_count55))

  xFF=collect(keys(status_countFF))
  yFF=collect(values(status_countFF))

  xCr=collect(keys(crashs_count))
  yCr=collect(values(crashs_count))

  traceFF = bar(;x=xFF,
                  y=yFF,
                  name="Correct")
  trace55 = bar(x=x55,
               y=y55,
               name="Faulty")
  traceCr = bar(x=xCr,
               y=yCr,
               name="Crashed")
  data = [traceFF, trace55, traceCr]
  layout = Layout(;barmode="stack")

  p = plot(data, layout) # to see interactively just plot(data, layout)
  PlotlyJS.savefig(p, "../../../../CARDIS2017/Figures/highjack.pdf")
  PlotlyJS.savefig(p, "../../../../CARDIS2017/Figures/highjack.png")
end
