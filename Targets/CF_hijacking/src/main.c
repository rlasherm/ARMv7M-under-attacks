/**
* @Author: helene, ronan
* @Date:   09-08-2016
* @Email:  helene.lebouder@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 19-09-2016
* @License: GPL
*/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "aes.h"
#include "status.h"
#include <string.h>

/** @addtogroup STM32F1xx_HAL_Examples
  * @{
  */

/** @addtogroup Templates
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static GPIO_InitTypeDef  GPIO_InitStruct;
UART_HandleTypeDef UartHandle;
const char *welcome = "Shall we play a game?\n";
uint8_t command[1];//single byte commands
char text[256];
unsigned char plaintext[DATA_SIZE];
unsigned char ciphertext[DATA_SIZE];
unsigned char key[DATA_SIZE]={121,27,229,29,20,83,109,221,136,255,194,140,136,168,13,101};
uint32_t status_value;


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void Error_Handler(void);

/* Private functions ---------------------------------------------------------*/
//#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
//#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
//#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
//#endif
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */



int main(void)
{
  int verbose = 0;
  /* STM32F1xx HAL library initialization:
       - Configure the Flash prefetch
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
  HAL_Init();

  /* Configure the system clock to 24 MHz */
  SystemClock_Config();


  /* -1- Enable each GPIO Clock (to be able to program the configuration registers) */
  LED3_GPIO_CLK_ENABLE();
  LED4_GPIO_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /* -2- Configure IOs in output push-pull mode to drive external LEDs */
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

  GPIO_InitStruct.Pin = LED3_PIN;
  HAL_GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LED4_PIN;
  HAL_GPIO_Init(LED4_GPIO_PORT, &GPIO_InitStruct);

  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  // Start the MCO outputing the main system clock (PA8)
  HAL_RCC_MCOConfig(RCC_MCO, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);

  /*##-1- Configure the UART peripheral ######################################*/
  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
  /* UART configured as follows:
      - Word Length = 8 Bits (8 data bit + 0 parity bit)
      - Stop Bit    = One Stop bit
      - Parity      = ODD parity
      - BaudRate    = 115200 baud
      - Hardware flow control disabled (RTS and CTS signals) */
  UartHandle.Instance        = USARTx;

  UartHandle.Init.BaudRate   = 115200;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX_RX;

  if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UART_Init(&UartHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_PIN, GPIO_PIN_RESET);

  //Send welcoming message
  HAL_UART_Transmit(&UartHandle, (uint8_t *)welcome, strlen(welcome), 0xFFFF);



  /* Infinite loop */
  while (1)
  {
    //read command
    if(HAL_UART_Receive(&UartHandle, &command[0], 1, 0xFFFF) == HAL_OK)
    {
      //HAL_GPIO_TogglePin(LED3_GPIO_PORT, LED3_PIN);

      switch (command[0]) {
        case 't'://test
          sprintf(text, "STM32 DIV Test OK\n");
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;

        case 'k'://set key (binary)
          HAL_UART_Receive(&UartHandle, (uint8_t *)key, DATA_SIZE, 0xFFFF);
          if(verbose)
          {
            sprintf(text, "Key set.\n");
            HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          }
          break;

        case 'p'://set Plaintext (binary)
          HAL_UART_Receive(&UartHandle, (uint8_t *)plaintext, DATA_SIZE, 0xFFFF);
          if(verbose)
          {
            sprintf(text, "Plaintext set.\n");
            HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          }
          break;

        case 'g'://go!
          AESEncrypt(ciphertext, plaintext, key);
          if(verbose)
          {
            sprintf(text, "Go.\n");
            HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          }
          break;

        case 'c'://get ciphertext
          HAL_UART_Transmit(&UartHandle, (uint8_t *)ciphertext, DATA_SIZE, 0xFFFF);
          break;

        case 'f'://fast mode
          HAL_UART_Receive(&UartHandle, (uint8_t *)plaintext, DATA_SIZE, 0xFFFF);//receive Plaintext
          AESEncrypt(ciphertext, plaintext, key);//AES
          HAL_UART_Transmit(&UartHandle, (uint8_t *)ciphertext, DATA_SIZE, 0xFFFF);//transmit ciphertext
          break;

        case 'r': //round Select
          HAL_UART_Receive(&UartHandle, (uint8_t *)text, 1, 0xFFFF);//receive Plaintext
          trigger_sel = text[0];
          //sprintf(text, "Round selected %d.\n", trigger_sel);
          //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;
        case 's':
          verbose = 0;
          break;
        case 'v':
          verbose = 1;
          break;
        case 'a':
          HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_PIN, GPIO_PIN_RESET);
          HAL_GPIO_WritePin(LED4_GPIO_PORT, LED4_PIN, GPIO_PIN_RESET);
          HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
          wait(1);
          status_value = test_persistence();
          // HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
          HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET);
	        HAL_UART_Transmit(&UartHandle, (uint8_t *)&status_value, 4, 0xFFFF);
          break;
        case 'q':
  	  HAL_UART_Transmit(&UartHandle, (uint8_t *)&status_value, 4, 0xFFFF);
          break;

        default:
          sprintf(text, "Unknown command: %c\n", command[0]);
          HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
          break;
      }

      //HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
      //blink_led4();

    }
    else
    {
      //sprintf(text, "Didn't heard\n");
      //HAL_UART_Transmit(&UartHandle, (uint8_t *)text, strlen(text), 0xFFFF);
    }
  }
}

void fast_trig_up()
{
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
}

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART1 and Loop until the end of transmission */
  HAL_UART_Transmit(&UartHandle, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 24000000
  *            HCLK(Hz)                       = 24000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            HSE PREDIV1                    = 2
  *            PLLMUL                         = 6
  *            Flash Latency(WS)              = 0
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV2;
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_0)!= HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  while (1)
  {
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
