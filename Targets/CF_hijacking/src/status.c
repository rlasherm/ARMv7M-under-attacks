/**
* @Author: kevin
* @Date:   11-05-2017
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 11-05-2017
* @License: GPL
*/

/* Includes ------------------------------------------------------------------*/
#include "status.h"
#include "stm32f1xx_hal.h"
#include "stm32vl_discovery.h"
#include "stdio.h"
#include <string.h>

// uint32_t set_status(void){
//   uint32_t status_value ;
//
//   status_value = 0x55555555 ;
//   return status_value ;
// }

uint32_t pin_correct = 1;

// uint32_t reset_status(void){
//   uint32_t previous_status, new_status;
//
//   HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);
//   previous_status = set_status() ;
//   new_status = 0xFFFFFFFF;
//   return  new_status ;
// }

uint32_t test_persistence (void){
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);
  uint32_t measured_status = 0;
  if(pin_correct == 1) {
    measured_status = 0xFFFFFFFF;
  }
  else {
    measured_status = 0x55555555;
  }
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

  return measured_status;
}

// uint32_t test_persistence (void){
//   uint32_t measured_status = reset_status();
//   HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
//   unsigned int counter;
//
//   if (measured_status == 0xFFFFFFFF){
//     HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_PIN, GPIO_PIN_SET);
//
//   }
//   else if (measured_status == 0x55555555){
//     HAL_GPIO_WritePin(LED4_GPIO_PORT, LED4_PIN, GPIO_PIN_SET);
//
//   } else {
//     HAL_GPIO_WritePin(LED3_GPIO_PORT, LED3_PIN, GPIO_PIN_SET);
//     HAL_GPIO_WritePin(LED4_GPIO_PORT, LED4_PIN, GPIO_PIN_SET);
//
//   }
//
//   return measured_status;
// }

void wait (int counter){
  int start, current;
  start = HAL_GetTick();
  start += counter;
  current = HAL_GetTick();
  while(start < current)
  {
    current = HAL_GetTick();
  }

}

/*
int set_key(uint32_t user_id, mbedtls_aes_context *ctx, unsigned char *key){
  key_context * key_test

  &key_test->key_id = user_id ;
  &key_test->key = key ;

  return 0;
}

void select_key (int key_id){
  if( key_id == &key_test->key_id){
    &key_test->key = key ;
  }
  // return key;
}
*/

/*
void init_keys (void) {


}
*/
