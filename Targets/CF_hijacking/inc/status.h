/**
* @Author: kevin
* @Date:   11-05-2017
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 11-05-2017
* @License: GPL
*/
#include "stm32f1xx_hal.h"

#ifndef STATUS_H
#define STATUS_H
/*
typedef struct
{
  int nr;                     /*!<  number of rounds
  uint32_t *rk;               /*!<  AES round keys
  uint32_t buf[68];           /*!<  unaligned data
}
mbedtls_aes_context;
*/

/* structure corresponding to each key
 * each key is identified by an id
 *//*
typedef struct
{
  int key_id ;                       /*key id
  mbedtls_aes_context *key_context ; /*key context
  unsigned char *key ;               /*key
}
key_context;
*/
/* set status value*/
uint32_t set_status(void);
/* reset status value*/
uint32_t reset_status(void);
/* test bench */
uint32_t test_persistence(void);
/* Set key value */
//int set_key(int user_id/*, mbedtls_aes_context *ctx*/, unsigned char *key);
/* select key from id */
//int select_key (int key_id);
/* init keys */
//void init_keys (void);
/*wait function*/
void wait (int counter);


#endif
