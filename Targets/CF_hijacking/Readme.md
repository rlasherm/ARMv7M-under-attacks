This application is an AES on STM32. Communication with the host computer is done with UART. We use the FTDIFriend module from ADAFruit to have UART2USB conversion.

This application also contain a test for status persitence after a Fault Injection.

Fault model is nope instruction or instruction skipped
**Targeted instruction is at 0x8000f9e mov r0, r3** (not available with -O2 compilation option)

Status to validate fault injected is 0x55555555

Tag:
* Nosideeffects: Introduce jitter on trigger, seen on video nosideeffects_fault.mp4  into experiments folder
  ** Amplitude: -12 dBm
  ** Burst: 4
  ** Delay: 1516us
  ** Period: 3.10ns
  ** Width: 1.5ns
  ** Lead Edge: 1.0ns
  ** Trail Edge: 1.0ns
  ** If CPU on the left and consider size of 9*9mm, probe on X=-4mm Y=5mm
