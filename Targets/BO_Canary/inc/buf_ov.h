/**
 * @Author: Lashermes Ronan <ronan>
 * @Date:   01-06-2017
 * @Email:  ronan.lashermes@inria.fr
 * @Last modified by:   ronan
 * @Last modified time: 01-06-2017
 * @License: MIT
 */



#ifndef BUF_OV_H
#define BUF_OV_H

#include <string.h>

char big_text[256+48];

/*wait function*/
void wait (int counter);

void init_big_text();
void copy_text(char* dest, size_t max_dest_size);


#endif
