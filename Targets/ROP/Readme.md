This application is an AES on STM32. Communication with the host computer is done with UART. We use the FTDIFriend module from ADAFruit to have UART2USB conversion.

This program allow to create a ROP by a fault injection

To test just compile it and execute on STM32

Send key, plaintext and go signal to generate a cipher value

Press d to generate stack status

Press e to generate jump to a useless function and send the fault

Expected beahaviour:

Fault injected during useless function cause a jump to test_rop which is a door opened upon stack.
Stack become instruction pointer and first jump to memcpy() using key as source and ciphertext as destination then jump to uart_transmit to send the ciphertext to user.


-(useless_function())- -> test_rop() -> memcpy(dest,src,length) -> 3 gadgets -> uart_transmit(cipher,length)

Prerequisities:

Stack filled with right values (function fill_stack)
Useful gadget following skipped jump (in our case contained in test_rop())
Source code full knowledge
