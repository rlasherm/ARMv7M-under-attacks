/**
 * @Author: Bukasa Kevin
 * @Date:   01-07-2017
 * @Email:  sebanjila.bukasa@inria.fr
 * @Last modified by:   kevin
 * @Last modified time: 20-07-2017
 * @License: MIT
 */



#ifndef ROP_H
#define ROP_H

#include <string.h>

void useless_function (void);

#endif
