/**
 * @Author: Bukasa Kevin
 * @Date:   01-07-2017
 * @Email:  sebanjila.bukasa@inria.fr
 * @Last modified by:   kevin
 * @Last modified time: 20-07-2017
 * @License: MIT
 */



#include "rop.h"
#include "stm32f1xx_hal.h"

void useless_function (void){
    wait(1);
    return;
}
