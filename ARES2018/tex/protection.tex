\section{Protections and countermeasures}
\label{sec:protection}

The destructive potentiality of \gls{EMFI} has been demonstrated. But there are several possibilities to protect a system.

\subsection{Formally proven applications}

We call a $k$-mutant a program where up to $k$ instructions have been skipped.
A popular way to protect any program against fault attacks (with only $1$ fault injected) is to formally prove that no $1$-mutant falsify some predefined security properties.

This approach is two folds: first the designer must be able to detect weaknesses with the prover, then in a second she would like an automatic way of generating a satisfying program.
In this regard, the work of Moro \textit{et al.} in~\cite{formal_secure} proposes an interesting, but costly, solution. In this paper, the authors propose to replace all Thumb-2 instructions with an equivalent sequence that can undergo any replacement of one instruction with a nop. By construction the resulting program can be proven secure against single nop fault injection. This work has been extended in~\cite{BarryCR16} by modifying the compilation chain instead of altering the binary.

Unfortunately, the formal method approach suffers from two major flaws.
First the security of an application cannot be considered separately from the rest of the system.
As an example, a cryptographic algorithm may be proven secure against fault injection but if a vulnerability is injected in some other part of the system (say the communication to the host), the adversary may be able to take control of the chip and simply read the key in memory (or ask decryption to the secure element).

Second, multiple faults are an experimental reality. If checking all $1$-mutants is hard, what about $2$-mutants, $3$-mutants, etc. Formally prove security becomes rapidly computationally hard and the probability that a $k$-mutant is unsecure increases with $k$ (the attacker has more power).

Additionally, real systems cannot be represented by our simplified model: programs are not self-contained and dynamically call code from (sometimes shared) libraries.
Interrupts may modify your program flow in a non deterministic way, Direct Memory Access (DMA) may modify the memory without your program controlling it.
Formal methods are not (yet) realistically able to model a real system.

\subsection{Add an external secure element}

A popular architecture today to add security to a system, it to add an external (can be embedded in the SoC) secure element (SE) in charge of all security related operations to a main processor or microcontroller. Cryptographic keys are thought protected from vulnerabilities since they are only present in a secure memory in the SE. In~\cite{eurisko}, a detailed implementation following this architecture is described.

Yet we think that one must be very careful with this kind of dual architecture: it has been shown in this paper that the main processor can be hijacked by an attacker with fault injection means.
This fact imposes new constraints on the role of the main processor with respect to the secure element.
If an attacker can seize control of the processor, it may legitimately ask anything to the SE.
Therefore, the security policy must absolutely be implemented in the SE, not in the main processor.
%Here is an example: an application requires a user to authenticate via a PIN in order to asks for the decryption of a message.
%
%A first implementation verifies, with all state-of-the-art countermeasures, the PIN on the main processor and if correct asks the decryption of the message to the SE. But as demonstrated in this paper, an attacker may execute arbitrary code on the main processor by creating a vulnerability in a system deemed not critical with respect to fault attacks. Once she has seized control of the system, the attacker can asks for the decryption without bothering with the PIN.
%
%A correct implementation would see the PIN verification done inside the SE. The whole logic must be protected with respect to physical attacks. There must be absolutely no impact on the security of the system if the main processor is compromised.

\subsection{Increasing experimental difficulty}
\label{sec:exp_diff}

This category of countermeasure relies on the fact that fault injection is an experimental process. One can make the life of the attacker miserable with quite simple tricks.
Desynchronization is probably the simplest one. Make sure that no instruction is executed twice at the same time with respect to each other. Use a randomized clock, where the clock period may change at each cycle.

The cache memory hierarchy is a natural countermeasure in this regard. Since it is hard to predict if a data is present in cache or will be missed (it would not be missed otherwise), resulting in different fetching timings, the program will have hardly any synchronous point.

In all cases, the experimental difficulty is the main limitation of \gls{EMFI}. The attacker has a low control on the injected faults, she does not know exactly what will be the new faulted instruction value ahead of time. The attack scenario must take this uncertainty into account.

\subsection{Fault attack detectors}

The best way to detect hardware fault injection is to add hardware detectors. In~\cite{glitch_detector}, the authors add several hardware components able to detect dynamically a bad timing behaviour.


\begin{figure}
\center{
\includegraphics[width=0.4\textwidth]{guard.png}}
\caption{Diagram of a fault attack detector.\label{fig:detect}}
\end{figure}

As shown on figure~\ref{fig:detect}, the detector generates a guard signal from the clock. This signal is high only if the clock can safely rise, no timing error will occur. A flip flop is used to detect the value of the guard signal at the rising edge of the clock, if not correct, an alarm is raised.

The main advantage of this solution is that all sources of bad timing can be detected: clock glitches, power underfeeding, temperature raising and \gls{EMFI}.

But lasers, that have a different fault injection mechanism, are not detected.


\subsection{Control Flow and System Integrity (CFI\&SI)}
\label{sec:cfi_si}

CFI intends to ensure the control flow correctness, by adding additional checks at critical points in the program. In~\cite{soft_detect,soft_counter}, the authors propose and evaluate such software-only schemes. Even if these works propose elegant solutions to the problem, they assume limited fault models (single-bit or single-nop) and are software only. It does not suit the complexity of the behaviours observed during our experiments.

Another strategy to protect any system is proposed in~\cite{sofia}. In this paper, the authors propose an implementation of a control flow and system integrity mechanism on a LEON3 core.
To ensure control flow integrity, the application is encrypted with Program Counter information. The next instruction can only be correctly decrypted if the program counter and the previous program counter are correct.

A simplified version would work as follows: to ensure control flow integrity of a code , all instructions are encrypted ahead of time with the following scheme.
\begin{equation*}
i' = E_k\left(PC || PC_{previous}\right) \oplus i
\end{equation*}
where $i$ is the instruction to encrypt, $\gls{PC}$ is the Program Counter value at instruction $i$, $PC_{previous}$ the previous Program Counter value, $E_k$ an encryption algorithm with key $k$ and $i'$ is the resulting encrypted instruction.

During execution, instructions are decrypted on-the-fly from the current and previous Program Counter values. If these are incorrect, it becomes impossible to execute the program. This simplified scheme can work only if all instructions have only one predecessor (not two calls to the same address). The case of more than one predecessor is dealt with in~\cite{sofia} as well as how to ensure system integrity.

In its current form, this scheme is hard to adapt to complex real life scenarios without prohibitive overhead. Yet we believe that this is the correct way forward to address the threat implied by fault injections attacks.

\subsection{Wishes for the future}

Today the only viable solution to secure a complete system is to use a Secure Element with as much security logic as possible implemented on it. But it does not protect from using an \gls{EMFI} to obtain privileged access to the main processor.

In the future, at least one of the core would have to be hardened, probably with an efficient CFI\&SI mechanism. All privileged code would have to run on it (the kernel in particular) so that the compromise of an unprotected core have only limited consequences. Similarly, an application will be able to ask for a secure execution on the hardened core for critical operations. For this scheme to succeed, the hardening cost must be limited in terms of performance.
A SE will still be used for dedicated functionalities such as cryptographic key management.
