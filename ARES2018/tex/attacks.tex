\section{Fault attacks}
\label{sec:attacks}

%% Proposition Kevin : Appuyer un peu plus sur l'application possible de ces attaques, elles peuvent entrainer tel ou tel fuite, même si pour l'instant c'est une preuve de concept

In this section, we will demonstrate how to create vulnerabilities in a sound codebase with fault injection.
Several use-cases have been experimentally implemented to illustrate the kind of vulnerabilities it is possible to achieve with fault attacks on real-world devices. Each scenario is based on well-known attacks scheme, and fault injection is a mean to activate a vulnerability.

\subsection{Controlflow hijacking}

\subsubsection{Description}
The control flow hijacking consists in forcing a program to follow a branch that it should normally not. A real world usage of this attack is a PIN code validation, if the given PIN is false, access should not be granted, ie. first condition is incorrect.

\begin{lstlisting}[language=C,caption={Targeted C code},label={code:status}]
if(correct == 1) {
	status = 0xFFFFFFFF; }
else {
	status = 0x55555555; }
\end{lstlisting}

\begin{lstlisting}[caption={Resulting assembly (thumb2)},label={code:status_asm}]
cmp	r3, #1						; r3 contains *correct*
ite	eq 							; if then else
moveq.w	r4, #4294967295		; 0xffffffff
movne.w	r4, #1431655765		; 0x55555555
\end{lstlisting}

From the C code on listing~\ref{code:status}, the normal behaviour is for \textit{status} to be equal to \verb|0xFFFFFFFF| after its execution. Our goal is to follow the ``else'' branch instead (we detect \textit{status} equal to \verb|0x55555555| and \textit{correct} still equal to 1).

Upon a correct fault, we reach an incoherent program state (that should not be reachable).

\subsubsection{Attack results}

To perform our attack, as in the other cases, we insert a triggering method just before our area of interest.
The search for the correct parameters is done semi-automatically: first interesting parameters (that crash the target) are found for the number of pulses, the pulses period, width, leading and trailing edges duration, and the pulse amplitude.
Then we scan the chip with a XY stage to detect the most sensitive location.
In front of the number of parameter dimensions to explore, a part of the work depends on the experience of the operator to find the correct parameters.

In the end a set of parameters is found that trigger the expected behaviour. The signal sent to the EM probe is a train pulse with $15$ pulses (period $3.1$ns, width $1.6$ns) with a $-9$dBm amplitude before the amplifier.

%\begin{figure}
%\center{
%	\includegraphics[width=0.5\textwidth]{highjack.pdf}
%	}
%	\caption{Rates of correct, faulty and crashed behaviours for various injection timings.\label{fig:highjack_fig}}
%\end{figure}

%On figure~\ref{fig:highjack_fig}, the result values (for 100 successful executions at each timing) can be seen for various injection timing values (outside of these timings, no interesting faulty behaviour were observed).

Faulty outputs (\verb|status=0x55555555|) are observed for $10\%$ of the executions during $\approx 200$ns (the timing of the fault injection varies), or $\approx 5$ instructions. Possible explanations are: several instructions may be faulted and giving the same faulty behaviour, a fault may be effective at different stage of the pipeline for a same instruction.
%The signal sent for the fault injection lasts $\approx 46$ns and may have the same impact when shifted in time if only a part of this signal is useful to create the fault.
%Finally a jitter may be present that lead to the vulnerable instruction to be at different timings with respect to our trigger.

As discussed in section~\ref{sec:fault_models}, it is not possible to precisely explain what is happening in the chip after a fault injection without altering the target program (and in this case we explain the fault behaviour for another program).

\subsubsection{Consequences}

This very simple pattern shows that with a fault attack, we may modify the control flow of our program \textit{dynamically}, and even reach an incoherent state (that should not be reachable).
Actually this pattern is already taken into account in the design of Verify PIN algorithms where fault attacks have been, rightly, considered a threat.

What this pattern reveals is that any branch in the code can be hijacked, easily leading to the compromise of the system. {All} branching must be protected or demonstrated innocuous.

\subsection{Buffer overflow}
\label{sec:bo}

\subsubsection{Description}
A buffer overflow is achieved when data is written outside the boundaries of the destination buffer during memory handling, leading to values written to adjacent memory locations. Here an attacker which can only access some variables can copy informations from internal unknown variables to readable ones.

Considerable effort have been devoted to mitigate this class of attacks in the past. %\TODO{ref}.
In this use-case, it is demonstrated that a buffer overflow vulnerability can be created in a sound program with a fault attack.
%Interestingly, we will show that this buffer overflow demonstration did not succeed as expected (the exact desired behaviour was not achieved) but another behaviour not usually considered in buffer overflow attacks was achieved leading to the program compromise.

Two different buffer overflows have been attempted. In the first attack (hereafter called \textit{BO1}), the targeted value \textit{key} is located close to the buffer written to (\textit{text}). In a second attack (\textit{BO2}), the two values are separated by a \textit{canary}: a special buffer that issues an error if modified. The layout can be seen on listing~\ref{code:bo_mem_layout}.

\begin{lstlisting}[language=C,caption={Memory layout targeted by the buffer overflow},label={code:bo_mem_layout}]
char text[n]						; // n=128 for BO1, n=8 for BO2
char canary[8]						; // BO2 only
unsigned char key[16]			;
\end{lstlisting}

The targeted function is a \textit{strncpy} that copy data from a big buffer called \textit{big\_text} filled with a distinctive pattern (\verb|01 02 03 04 ...|) to \textit{text} with the correct size parameter \textit{n}.
Our objective is to modify the \textit{key} by disturbing the correct \textit{strncpy} behaviour.

% \begin{lstlisting}[caption={Assembly code for \textit{strncpy} call},label={code:strncpy call}]
% ; pass n as argument
% mov	r2, r5
% ; get destination (text) address
% mov	r0, r6
% ; get source (big_text) address
% ldr	r1, [pc, #24]
% ; call strncpy procedure
% bl	8004770 <strncpy>
% \end{lstlisting}

\begin{lstlisting}[caption={Assembly code for \textit{strncpy} call},label={code:strncpy call}]
mov	r2, r5			; pass n as argument (in r2)
mov	r0, r6			; get destination (text) address (in r0)
ldr	r1, [pc, #24]	; get source (big_text) address (in r1)
bl	8004770 <strncpy>; call strncpy procedure
\end{lstlisting}
As can be seen on listing~\ref{code:strncpy call}, if the instruction loading the n argument could be replaced by a nop, then the actual \textit{n} value will depend on the value of the \verb|r2| register at this call. If the new value is bigger than the previous one, a buffer overflow becomes possible. But if this new value is bigger than the source buffer, our memory will be filled with zeros according to \textit{strncpy} documentation.
But fault attacks allow us to modify any instruction, so we can also try to alter source or destination addresses.

\subsubsection{Attack results}

First, we confirm that preventing the proper loading of the size limiting argument (\textit{n}) does not work. A nop was achieved, but the value stored in \verb|r2| was an address (\verb|0x2000xxxx|) and as specified in the documentation \textit{n} bytes are always written to the destination buffer. First the source buffer, then a null padding. The fault, in fact wiped out our RAM, triggering a crash (probably by writing to unmapped addresses).

%Several other tries were made for the \textit{BO1} campaign, by forcing \verb|r2| to a more interesting value, but the expected result was not observed (the expected ciphertext with the modified key was precomputed). Instead another faulty ciphertext was found.


By methodically tuning the delay parameter in order to fault successively various instructions, a faulty ciphertext was finally found.
OpenOCD was used, as explained in section~\ref{sec:instrumentation}, to observe the memory at the \textit{text} and \textit{key} locations. Figure~\ref{fig:bo_ocd} shows the result.

\begin{figure}[h]
\center{
\includegraphics[width=0.9\columnwidth]{bo.png}
}
\caption{Memory observed with OpenOCD after a successful fault injection. The correct \textit{text} address is 0x20000918, the first 4 words are correct, resulting from a previous unfaulted execution. The \textit{key} address is 0x20000a18.\label{fig:bo_ocd}}
\end{figure}
It shows that \textit{n} was not modified. Instead, the destination address was altered and the \textit{big\_text} buffer was written at the wrong location (16 bytes shift). Finally, the \textit{key} was overwritten at the end of the buffer. A buffer overflow was achieved.

A buffer overflow is usually achieved by modifying the number of bytes written. Here the destination address is modified. This fact motivated our second \textit{BO2} campaign, where a canary is present in memory to detect buffer overflows.
This canary is manually implemented in this case, but nowadays compilers are able to place them automatically, often at stack frame boundaries.
In our application, if the canary is modified, the key is erased to $0$ which can easily be detected by our platform.

The \textit{BO2} campaign explored a big timing range to see what effect could be achieved. The most important point is that the \textit{key} is not used in any way by the targeted function (\textit{strncpy}), only \textit{text} which is close in memory.

The results show that the canary has been modified in numerous cases, thus the attack failed in these cases. But faulty results were achieved nonetheless. Using OpenOCD, the \textit{key} value was observed: the first word was replaced by either \verb|0x08000000| or \verb|0x00008000| depending on the fault injection timing. This is not the result of a misplaced buffer, but the exact cause of this error is still unknown. It is reminded that the targeted code does not handle memory at the \textit{key} address, it cannot be explained by a fault at the decode stage on a load instruction.

\subsubsection{Consequences}

Unlike pure-software buffer overflows, it is possible with hardware fault injection to modify the size of the memory copy but also to alter the destination address. It has been demonstrated that it can jump over a canary, a classic countermeasure against buffer overflows.
Yet the \gls{EMFI} does not allow a good control on the fault effect.
%There is a real need for a systematic exploration of the fault injection consequences on the application behaviour to explain what caused the observed results.


\subsection{Fault activated backdoor}

\subsubsection{Description}

In this use-case, it is supposed that a malevolent insider (with access to source code) tries to add a backdoor into a cryptographic application. In order to not be detected by static analysis tools, the backdoor payload cannot be accessed normally by the code (no branch leads to the payload, it cannot be normally executed). As dead code, it can be detected as such but it can also be hidden as data. Yet a precisely timed fault injection is able to activate the payload.

The backdoor is split in two parts: the payload copies the key into the ciphertext buffer. The backdoor trigger (cf listing~\ref{code:backdoor_trigger}) is an (almost) innocent looking code that can launch the payload upon a fault injection.

\begin{lstlisting}[language=C,caption={Backdoor trigger},label={code:backdoor_trigger}]
void blink_wait()
{
  unsigned int wait_for = 3758874636;
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
\end{lstlisting}

The backdoor trigger compiles to the assembly code show on figure~\ref{code:backdoor_trigger_asm} (only the end of the function is shown).

\begin{lstlisting}[caption={Backdoor trigger (assembly)},label={code:backdoor_trigger_asm}]
	...
 80005ca:	bd80      		pop	{r7, pc}
 80005cc:	e00be00c 		.word	0xe00be00c
\end{lstlisting}

This backdoor trigger lies in the fact that the ARMv7-M interleaves data and code if a data too big to be embedded in an instruction is used.
Thus \verb|3758874636| corresponds in fact, if executed, to two instructions that jump to the backdoor: \verb|e00b e00c|.
A fault instruction able to avoid the \textit{blink\_wait} return instruction at address \verb|0x80005ca| implies that the data at \verb|0x80005cc| is executed. A covert backdoor trigger is obtained.
In our program the backdoor payload is in plain sight, but one could imagine sneakier ways to hide it.

\subsubsection{Attack results}

With an injection timing of $2.116 \mu s$, the attack succeeded with a low success rate of $4\%$, in the other $96\%$ nothing happened and the result was correct.

The value \verb|00112233445566778899AABBCCDDEEFF| (key value) is observed as the ciphertext when the backdoor has been triggered.
To verify that the backdoor trigger was responsible for the backdoor call, a hardware breakpoint was placed at address \verb|0x80005cc| which is normally a data (and so never executed). As expected, on a successful fault injection the breakpoint did halt the execution and a ``step'' in debug mode did show a jump to the backdoor. The triggering path has been validated. Unfortunately, we have no way to be sure that a nop is indeed responsible for the data execution at \verb|0x80005cc|.

But something else, strangely, did appear during the experiments. At $2.268 \mu s$, a faulty value \verb|279FB74A445566778899AABBCCDDEEFF| is observed (first word is correct ciphertext, the 3 other words correspond to the key). To investigate, a breakpoint was placed at the end of the backdoor payload.
Indeed this breakpoint was reached.
By reading the memory at this point, part of the program path leading to the backdoor payload was reconstructed. The backdoor trigger was not responsible but in a neighbouring part of the code, an address corresponding to the middle of the backdoor was found in the stack. A stack corruption leads to a jump to this location through a \textit{pop \{..., pc\}}.
How this address ended up in the stack is still a mystery (and unfortunately cannot be explained cf section~\ref{sec:fault_models}).

\subsubsection{Consequences}

With this use-case it has been showed that a backdoor may be hidden in an inert part of the memory. As such, static analysis tools cannot detect this vulnerability with information flow analysis.
This vulnerability is a reminder of the well known problem of executable data. Usual protection may be efficient in this case apart for the peculiarity of ARMv7-M to interleave some data with executable code.

\subsection{Return Oriented Programming}

\subsubsection{Description}

In this use-case, a \gls{ROP} exploit is demonstrated.
From her knowledge of the binary, an attacker tailors her own program by imposing her control flow graph on top of existing code. It becomes possible to take full control of a targeted device, including execution of privileged instructions.
If the binary is big enough, code sections with a useful behaviour will always be present.
\gls{ROP} allows to use these sections and to chain them in a useful manner to achieve the desired behaviour and minimize side effects.
These pieces of code are called ``gadgets''.
Their common particularity is that they include a useful behaviour for an attacker, and the sequence of instructions finishes by a return.
There are two different ways to ``return'' in ARMv7-M, namely \verb|bx lr| or \verb|pop {...,pc}|, the first one takes into account the \gls{LR} whereas the second take values from the stack. The second class of return is the most practical in our case, because with full control on the stack it is easier to pop from stack than to assign a value to \gls{LR}.
This attack is interesting for an attacker because it allows to use existent code to create a different behaviour, thus keeping static integrity.


\begin{lstlisting}[caption={Example of gadget (assembly)},label={code:gadget_example_asm}]
800039c:bd04      pop {r2, r5}
800039e:46ae      mov lr, r5
80003a0:bd00      pop {pc}
\end{lstlisting}

In listing~\ref{code:gadget_example_asm} registers r2, r5 and lr are filled with values from the stack, then the last pop is a branch by filling the \gls{PC} register with the next stack value.

If the attacker is able to branch to a controlled part of the stack, she can then use values that will call gadgets and then return to the stack.
The challenge here is to gain control of the stack and fill it with the right values, to activate the ROP.
As the attacker only manipulate addresses there are no modifications on the source code itself.
But she must know all exact addresses and necessary values to fill correctly the stack.

The \gls{ROP} goal is to copy the key value into the ciphertext buffer then to hand over to the normal control flow (similar to the backdoor scenario). So when reading the ciphertext after a successful fault injection, the key is read instead.

\subsubsection{Attack results}

Activating the \gls{ROP} has some prerequisites.
First of all, the stack must be filled with the \gls{ROP} data (the branches to gadgets) and the \gls{SP} correctly positioned.

In our setup, the stack has been filled at the current stack pointer with the adequate values. In a real attack, this part may be tricky.


\begin{lstlisting}[caption={\gls{ROP} activation (assembly)},label={code:rop_outbreak_asm}]
 80003dc   b.n 80003ec <some_function>
 80003de   nop
        ...
080003e2 <first_gadget>:
 80003e2   ldr r4, [pc, #4]; (80003e8)
 80003e4   mov lr, r4
 80003e6   pop {r0, r1, r2, pc}
 80003e8 .word 0x0800039d
\end{lstlisting}

As our test application is not big enough, some gadgets where missing. In particular, the first one has to be manually added: it saves the \gls{LR} required to return to the normal program flow after the attack. Then it sets the \gls{PC} from a value in the attacker controlled stack to the next gadget.

The \gls{ROP} activation happens when the program flow reach a function preceding the first gadget in memory as shown in listing~\ref{code:rop_outbreak_asm}.
Instruction at \verb|0x80003dc| is skipped.

A NOP is present in listing~\ref{code:rop_outbreak_asm} to illustrate a difficulty with fault injection. As clearly explained in~\cite{EM_model}, the fetch stage always load 32-bits of instruction (here corresponding to two instructions). Hence a fault may alter two instructions simultaneously depending on the alignment of the instructions, increasing the probability to get a crash.
Indeed in our first experiments, the alignment was not correct and caused the fault to impact two instructions: the first instruction of the first gadget was not properly executed.

The \gls{ROP} attack consists in filling the proper data in registers before calling \textit{memcpy} to copy the key into the ciphertext buffer. Since our gadgets do not corresponds exactly to the desired program, padding values are placed into the stack so that the values of interest for the attack are properly located into the stack

The last gadget is a return to normal execution. Finally, the attack succeeded as the key was read in the ciphertext buffer, with no error on other functionalities.

\subsubsection{Consequences}

This use-case demonstrates that the previous attack schemes may be leveraged into a fully fledge attack, namely a \gls{ROP} attack to execute an arbitrary payload designed by the attacker.

\gls{ROP} is a real threat for devices as it allows to take full control of a target by completely hijacking the control flow.

This technique is powerful on several account. First, the \gls{ROP} keeps the privilege the program has at the faulted instruction. It is a practical opening for privilege escalation. Then the static integrity is preserved before and after the attack. A forensic analysis cannot reconstruct easily what happened (if the stack is cleared after the attack).

%This technique take also advantage of several things, shirking privilege escalation by obtaining directly privilege linked with piece of code, indiscernible after execution even via forensic, dynamic so possible to create several classes of behaviour (such as simulate normal behaviour and adding some functions, etc.). Some techniques which include CFI \ref{sec:cfi_si} must be efficient to protect against ROP.

%\subsection{Lessons learned}
%
%With these simple use-cases, the power of \gls{EMFI} as a vulnerability injection tool has been demonstrated. What is particularly striking is that it is possible to have an impact on arbitrary memory locations, as demonstrated with the buffer overflow over a canary or the backdoor activation with a jump in the middle of it.
%
%Stack corruption is one of the most observed effect resulting from \gls{EMFI}. Most of the time, a stack corruption will lead to the crash of the system, but some other times an unexpected effect arises (e.g. a jump to some location whose address is in the stack).
%
%It is hard to control the effect of the fault, but one cannot rules out the possibility that a better setup, better parameters could improve this. And it is hard to predict the faulty behaviour ahead of time. Tools would be needed for that to accurately simulate the whole system and the dynamic alteration of one or a few instructions.
%
%Increasing the control power of the attacker would require to have a precise idea of the exact effect of the fault injection on the chip, but this problem is a whole research subject in itself.
