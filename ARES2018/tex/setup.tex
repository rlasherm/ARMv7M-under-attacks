\section{Fault injection process}
\label{sec:setup}

\subsection{Experimental setup}

Experiments have been performed in our laboratory. 
The targeted board is an STM32VLDISCOVERY board with an STM32F100RB chip, embedding an ARM Cortex-M3 core running at $24$MHz ($41.7$ns period).% as illustrated on Figure~\ref{fig:setup}.

% \begin{figure}[h]
% \center{
% \includegraphics[width=\columnwidth]{setup.png}
% }
% \caption{The probe positioned on the targeted chip.\label{fig:setup}}
% \end{figure}

Fault injection is performed with a signal forming chain consisting in a Keysight 33509B pulse generator, a Keysight 81160A signal generator and a Milmega 80RF1000-175 power amplifier. The created powerful signal is connected to a Langer RF probe RF B 0.3-3 located on the targeted chip.

In order to launch a fault injection, a synchronization signal (a trigger) is sent by the targeted chip \gls{GPIO} (controlled from the code) directly to the 33509B pulse generator. This experimental trick, possible when the attacker has control of the code (\textit{i.e.} never but for vulnerability assessment) is not mandatory. Other synchronization possibilities include sniffing communications with the target or measuring its EM emissions to find a relevant pattern.

The location of the probe on the chip was chosen after a scan that determined the most sensitive area on the chip. The same location was kept for all experiments.

Synchronization is the main experimental difficulty with our target. An irreducible latency of $600$ns is added by our fault injection platform between the input trigger and the EM pulse.

%Additionally, cache memories are present in this chip which create jitter (timing variance). It becomes hard to precisely target temporally one instruction; the later the targeted instruction with respect to the trigger, the harder.

The Sparkbench~\cite{sparkbench} software was used for the fault injection (open source, MIT license, see in reference). It is in charge of controlling the apparatus, orchestrating the commands to the target, retrieving the results and processing them. It includes a way to reset the target upon a crash and more generally to deal with errors that occur during a fault injection campaign.

\subsection{Instrumentation}
\label{sec:instrumentation}

The targeted chip is compatible with OpenOCD~\cite{openocd}, a tool that allows a debug access to the chip through a JTAG interface. In particular, it is possible to set breakpoints (the chip halts if a particular memory address is about to be executed), and to arbitrarily read in memory.

This tool was used to infer the behaviour of the chip upon a fault. Unfortunately, only partial information is available to us, there is no way to trace all previously executed instructions to the best of our knowledge.

% It should be stressed out that on the targeted board, a dedicated chip is in charge of the JTAG interface, separated from the microcontroller. This is why faults do not propagate to the debugging system.

When temporally scanning the chip (the injection timing sweeps over a predefined range) crashes occur frequently. %OpenOCD allowed us to verify that {these crashes were never}, with our parameters, {of electrical nature but could always be explained by the program corruption}.
%Observed reasons for a crash include reading in memory outside of the RAM boundary (invalid address) or jumping to an address not corresponding to a valid program.
OpenOCD allowed us to verify that these crashes were always explainable by software modifications e.g. reading in memory outside of the RAM boundary (invalid address) or jumping to an address not corresponding to a valid program.

Sparkbench was configured to issue a report upon a crash. This report include the timing of the crash, the \gls{SP}, and the \gls{PC} causing the software fault (in the ARM hard fault meaning).
It was used to get a feedback on the memory location of the fault, but approximately, since the software fault often occurs after the hardware one. E.g. if the stack is corrupted by a fault injection (hardware fault), the program may still run for a few instructions before reaching a point where the corrupted stack provokes an incorrect value to be loaded into the \gls{PC} (software fault).


\subsection{Unobservability of the fault model}
%\subsection{Fault models}
\label{sec:fault_models}

The effect of a fault on the chip is of critical importance both for the attacker and for the defender, it is the so-called fault model.
A detailed fault model as been proposed for our targeted chip in~\cite{EM_model}.
Moro \textit{et al.} explain that timing based fault injection (\gls{EMFI} included) is able to modify the instruction fetched. If the new instruction has no side effect, a virtual nop (No-operation, an instruction that does nothing) is obtained that cannot easily be differentiated with a true nop. If the new instruction has side effects, the result can be unpredictable.

It is important to notice that to establish this fault model, the observability difficulties are balanced by the controllability. I.e. since it is not possible to observe in details what is happening at the micro-architecture level, the authors carefully put the chip in the desired state (e.g. using nops to have an empty pipeline) to retroactively deduce the fault mechanism.

To our knowledge there is no better way to establish a detailed fault model on this chip. In particular it is not possible to know what instructions have really been executed upon a fault (execution traces are branch traces only). Yet it is also unsatisfying: what guarantee is there that another undocumented behaviour is exhibited if the chip is in another state (behaviour already proved in~\cite{Bx86ISA})?

The evaluator is left with a choice: either force the chip state to explain some of the fault mechanisms or do not alter the chip state, allowing all faulty behaviours, but leaving her unable to explain the fault mechanism.

If most previous works chose the first path~\cite{Dureuil2016,EM_model,fa_ARMv7M}, here the second one has been taken. As seen in section~\ref{sec:bo}, complex behaviours did arise.
Yet as unsatisfying as it is, {it becomes impossible to explain precisely some fault mechanisms} since we cannot trade controllability for observability.

{In this paper, the details of the fault mechanism leading to the observed behaviour is not explained since it is impossible without altering the targeted software.} Which, in the end, is an explication of the fault mechanism for another target program. The reader is referred to ~\cite{EM_model}, for a plausible fault model for this exact same chip.
